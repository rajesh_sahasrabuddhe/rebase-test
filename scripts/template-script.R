# Copyright statement comment
# ------------------------------------------------------------------------------
## Copyright 2015 Oliver Wyman Actuarial Consulting
  
# Author Comment
# ------------------------------------------------------------------------------  
#HELLO REBASE

## Prepared by <First Name Last Name (email)> 

# File Description comment, including purpose of program and inputs/assumptions
# ------------------------------------------------------------------------------


# set working directory
# Always set your working directory to the root folder of the analysis
# the absolute location of this folder will vary by analysis
# and computer


# source() and library() statements
# ------------------------------------------------------------------------------
#library(readxl)
#library(ggplot2)
#library(scales)
#library(dplyr)
#library(actuar)
#library(tidyr)
#library(devtools)
#library(owactools) # devtools::install_bitbucket("owac/owactools", 
#                   #   auth_user = "<BitBucket username>", 
#                   #   password = "<BitBucket password>")
#library(lubridate)
#library(Rdym) # devtools::install_github("wrathematics/Rdym")

# Function definitions
# -----------------------------------------------------------------------------

# Executed statements
# -----------------------------------------------------------------------------

