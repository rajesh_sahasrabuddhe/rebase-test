# R Analysis Template #

This is the OWAC template for an R analysis

## How to get started

1. Fork and rename this repository.
2. Edit this README to be appropriate for your new project.
3. Clone to your local computer and start customizing.